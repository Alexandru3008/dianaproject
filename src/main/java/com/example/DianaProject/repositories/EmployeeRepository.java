package com.example.DianaProject.repositories;

import com.example.DianaProject.entities.EmployeeEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface EmployeeRepository extends CrudRepository<EmployeeEntity, Long> {

}
