package com.example.DianaProject.repositories;

import com.example.DianaProject.entities.SalesEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SalesRepository extends CrudRepository<SalesEntity,Long> {

    @Query(value = "SELECT * FROM TBL_SALES WHERE Quantity >= ?1", nativeQuery = true)
    List<SalesEntity> findAllAbove(Long minSaleValue);

    @Query(value = "SELECT COUNT(s) FROM TBL_SALES s WHERE s.price >= ?1", nativeQuery = true)
    int expensiveProducts(Long min);

}
