package com.example.DianaProject.services;

import com.example.DianaProject.entities.SalesEntity;
import com.example.DianaProject.repositories.SalesRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SalesService {

    private final SalesRepository salesRepository;

    public SalesService(SalesRepository salesRepository) {
        this.salesRepository = salesRepository;
    }

    public List<SalesEntity> findAll()
    {
        List<SalesEntity> result = (List<SalesEntity>) salesRepository.findAll();

        if(result.size() > 0) {
            return result;
        } else {
            return new ArrayList<SalesEntity>();
        }
    }
}
