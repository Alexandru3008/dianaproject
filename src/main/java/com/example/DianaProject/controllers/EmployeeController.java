package com.example.DianaProject.controllers;


import com.example.DianaProject.entities.EmployeeEntity;
import com.example.DianaProject.services.EmployeeService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping(value = "api/employee")
public class EmployeeController
{

    private final EmployeeService service;

    public EmployeeController(EmployeeService service) {
        this.service = service;
    }

    @GetMapping("/getEmployees")
    public List<EmployeeEntity> getAllEmployees(){
        return service.getAllEmployees();
    }

}
