package com.example.DianaProject.controllers;


import com.example.DianaProject.entities.SalesEntity;
import com.example.DianaProject.services.SalesService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api/sale")
public class SalesController {

    private final SalesService salesService;

    public SalesController(SalesService salesService) {
        this.salesService = salesService;
    }

    @GetMapping("/getSales")
    public List<SalesEntity> getAllSales()
    {
       return salesService.findAll();
    }
}
