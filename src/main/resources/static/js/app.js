/*global angular*/
"use strict";
var app = angular.module('app', ['ngRoute','angularUtils.directives.dirPagination']);

app.config(function ($routeProvider, $locationProvider) {
    //configurare rute
    $locationProvider.hashPrefix('');
    $routeProvider
        .when('/sales', {
            templateUrl: '/list-sales.html',
            controller: 'sales'
        })
        .when('/employees', {
            templateUrl: '/list-employees.html',
            controller: 'employees'
        })
        .otherwise ({
            redirectTo: '/'
        });
});

app.run(function ($rootScope, $http) {

});
