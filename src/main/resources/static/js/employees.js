/*global angular*/
"use strict";
// var app = angular.module('app', []);

app.controller('employees', function($scope,$http){

    $http({
        method: 'GET',
        url: 'api/employee/getEmployees'
    }).then(function (response){
        $scope.employeeList = response.data;
        console.log("success!");
    },function (error){
        console.error('Error occurred:', error);
    });

});