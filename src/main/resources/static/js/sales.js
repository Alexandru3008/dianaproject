/*global angular*/
"use strict";
// var app = angular.module('app', []);

app.controller('sales', function($scope, $http){

    $scope.sortKey = 'id';
    $scope.reverse = false;

    $http({
        method: 'GET',
        url: 'api/sale/getSales'
    }).then(function (response){
        $scope.salesList = response.data;
        console.log("success!");
    },function (error){
        console.error('Error occurred:', error);
    });

    $scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }

});